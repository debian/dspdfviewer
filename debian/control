Source: dspdfviewer
Section: text
Priority: optional
Maintainer: Danny Edel <debian@danny-edel.de>
Build-Depends: cmake,
               debhelper-compat (= 13),
               libboost-dev,
               libboost-program-options-dev,
               libboost-test-dev,
               libpoppler-qt5-dev,
               pkgconf,
               qtbase5-dev,
               qttools5-dev,
               qttools5-dev-tools,
               xauth,
               xvfb
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/dspdfviewer.git
Vcs-Browser: https://salsa.debian.org/debian/dspdfviewer
Homepage: https://dspdfviewer.danny-edel.de
Rules-Requires-Root: no

Package: dspdfviewer
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: latex-beamer
Description: Dual-Screen PDF Viewer for LaTeX-beamer
 This is a specialized PDF Viewing application custom-made for
 the LaTeX class beamer, specifically the
 "show notes on second screen=right" option.
 .
 To make use of this program, you will need a document created
 by latex-beamer, and you will need two monitors connected to
 your computer.
 They do not need to have the same resolution, not even the same
 aspect ratio.
 .
 This program will split your PDF page in half, and display the
 left half (intended for the audience) on one monitor (think:
 a notebook's VGA output connected to your university's projector)
 and it will display the right half (intended for you) on the
 second screen.
 Also, on the second screen, you get page thumbnails and status
 information, like the time since you started the presentation
 and a "wall clock".
