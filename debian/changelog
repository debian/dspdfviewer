dspdfviewer (1.15.1+git20230427.d432d8d-1) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Migrate repository from alioth to salsa.

  [ Andreas Tille ]
  * Point watch file to latest Git commit
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Replace transitional package pkg-config by pkgconf in Build-Depends
    (routine-update)
  * Secure URI in copyright format (routine-update)
  * Rules-Requires-Root: no
  * Use secure URI in Homepage field.
  * Re-export upstream signing key without extra signatures.

 -- Andreas Tille <tille@debian.org>  Tue, 24 Dec 2024 21:21:55 +0100

dspdfviewer (1.15.1-1) unstable; urgency=medium

  * New upstream point release
    * Fixes issue with recent Qt5 where mousewheel would advance
      two slides insted of one

 -- Danny Edel <debian@danny-edel.de>  Thu, 15 Sep 2016 07:43:12 +0200

dspdfviewer (1.15-2) unstable; urgency=medium

  * add patch: fix FTBFS with plus in version number
  * Bump Standards-Version to 3.9.8 (no changes needed)

 -- Danny Edel <debian@danny-edel.de>  Sat, 06 Aug 2016 09:00:20 +0200

dspdfviewer (1.15-1) unstable; urgency=medium

  * Import new upstream version 1.15
    * Testsuite temporarily disabled on big-endian, see
      debian bug #816081 for details
  * Drop patches which have been applied upstream
    * Don't link against boost::test
    * Change fullscreen order
  * d/control: Update standards version to 3.9.7 (no changes needed)
  * d/control: Change Vcs-Git URL to https protocol
  * d/control: Update maintainer email address

 -- Danny Edel <debian@danny-edel.de>  Tue, 05 Apr 2016 12:27:03 +0200

dspdfviewer (1.14-2) unstable; urgency=low

  * Build package against qt5
  * Clarify build-depends:
    * Add dependencies on texlive-latex-recommended and latex-beamer
  * add patch: Don't link main binary against boost test
  * add patch: Change fullscreen order
  * No longer build a manual -dbg package, since automatic -dbgsym
    packages are here!

 -- Danny Edel <mail@danny-edel.de>  Sat, 30 Jan 2016 20:39:24 +0100

dspdfviewer (1.14-1) unstable; urgency=medium

  * New upstream version
    * Supports translation (german language file included)

 -- Danny Edel <mail@danny-edel.de>  Tue, 01 Dec 2015 15:51:40 +0100

dspdfviewer (1.13.1-2) unstable; urgency=medium

  * Allow parallel building (Closes: #803655)
  * Remove debian/menu file
  * Move package to collab-maint

 -- Danny Edel <mail@danny-edel.de>  Tue, 03 Nov 2015 10:22:27 +0100

dspdfviewer (1.13.1-1) unstable; urgency=medium

  * Initial release (Closes: #794319)

 -- Danny Edel <mail@danny-edel.de>  Thu, 06 Aug 2015 13:52:02 +0200
